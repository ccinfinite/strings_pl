# CCI-UŚ

***This is the repository for the developement of University of Silesia's part in the CCI project.***

# O1B

Specialised methods for non-IT arts and sciences.

This project will consist of 6 lessons for string manipulation.

* First three discuss basic string manipulation.
* Last three discuss NLTK library.
* Each lesson will have 5 questions which will accompany the content.
